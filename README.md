Traefik
=========

deploy docker based traefik as router, ssl endpoint and load_balancer

Requirements
------------

* ansible version `=~ 2.10`

Role Variables
--------------

| Name | Description | Default
| --- | --- | ---
| traefik_version | tag of docker image | v2.4
| traefik_folder | target folder where configuration will exist | /opt/traefik
| traefik_container_name | container name | traefik
| traefik_dashboard_user | user with access to traefik dashboard | test
| traefik_dashboard_password | password for dashboard user (should be vaulted) | test
| traefik_providers | provider definition  | docker and file provider
| traefik_entrypoints | entrypoints definition | 80 and 443
| traefik_loglevel | loglevel of traefik | INFO
| traefik_labels | labels for traefik container | as stated in defaults
| traefik_additional_config | additional config keys for traefik.yml | `{}`
| traefik_access_logs | access log configuration | as stated in defaults


Dependencies
------------

* installed docker daemon
* `docker` and `docker-compose` python modules installed

Example Playbook
----------------

    - hosts: servers
      roles:
        - role: traefik
          traefik_version: v2.4
          traefik_folder: /opt/traefik
          traefik_dashboard_user: admin
          traefik_dashboard_password: admin

License
-------

BSD

Author Information
------------------

tbd
